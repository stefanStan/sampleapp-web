import {Component} from '@angular/core';
import {AuthorisationService, NewUser} from "../../commons/authorisation.service";
import {Header} from "../../commons/header/header.component";

export class NewUserImpl implements NewUser {
  username: string;
  password: string;
  passwordConfirm: string;
  email: string;
  firstName: string;
  middleName: string;
  lastName: string;

  constructor(){
    this.username = "";
    this.password = "";
    this.passwordConfirm = "";
    this.email = "";
    this.firstName = "";
    this.middleName = "";
    this.lastName = "";
  }
}

@Component({
  selector: "register",
  template: require("./register.component.html"),
  providers: [AuthorisationService],
  directives: [Header]
})
export class Register {
  private newUser: NewUser;

  constructor(
    private authService: AuthorisationService
  ) {
    this.newUser = new NewUserImpl();
  }

  public register() {
    console.log(this.newUser);
    this.authService.register(this.newUser, console.log, alert);
  }
}
