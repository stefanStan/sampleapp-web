import {Component} from '@angular/core';
import {AuthObj} from "../../commons/authorisation.service";
import {Header} from "../../commons/header/header.component";
import {AuthorisationService} from '../../commons/authorisation.service';
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  template: require('./login.component.html'),
  providers: [AuthorisationService],
  directives: [Header]
})
export class Login {
  private authObj: AuthObj;

  constructor(
    private authService: AuthorisationService,
    private router: Router
  ) {
    this.resetFields();
    this.authService.resetSecurityContext(null);
  }

  public login() {
    this.authService.login(this.authObj, (x) => {
      console.log(x);
      this.router.navigateByUrl('sampleapp/roles');
    }, (x) => {
      console.error(x);
      alert(x.message);
    });
  }

  private resetFields() {
    this.authObj = {
      username: "",
      password: ""
    };
  }
}
