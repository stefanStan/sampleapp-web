import {Component} from "@angular/core";
import {RolesManagementService, Role} from "./roles.management.service";
import {Header} from "../../commons/header/header.component";
import {StorageService} from "../../commons/storage.service";
import {UserService} from "../../commons/user.service";

export class RoleImpl implements Role {
  key: string;
  name: string;
  description: string;

  constructor() {
    this.key = "";
    this.name= "";
    this.description = "";
  }
}

@Component({
  selector: "roles-management",
  template: require("./roles.management.component.html"),
  providers: [StorageService, RolesManagementService, UserService],
  directives: [Header]
})
export class RolesManagement {
  private roles: Array<Role>;

  constructor(
    private storageService: StorageService,
    private userService: UserService,
    private rolesManagementService: RolesManagementService
  ) {
    this.roles = [];
  }

  getUserRoles() {
    let userId = this.storageService.getSecurityContext().userId;

    this.userService.getUserRoles(userId, console.log, console.error);
  }
}
