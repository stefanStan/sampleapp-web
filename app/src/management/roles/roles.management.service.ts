import {Injectable} from "@angular/core";
import {Http} from "@angular/http";

const BASE_URL = 'http://localhost:8080/sampleapp-private-api/api/rest/cxf';

export interface Role {
  key: string,
  name: string,
  description: string
}

@Injectable()
export class RolesManagementService {

  constructor (
    private http: Http
  ) {

  }

  public getRoles(userId: number): Array<Role> {
    return [];
  }
}
