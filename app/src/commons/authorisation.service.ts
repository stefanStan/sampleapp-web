import {Http, Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {StorageService} from "./storage.service";

const BASE_URL = 'http://localhost:8080/sampleapp-private-api/api/rest/cxf';
const HEADER = { headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }) };

export interface SecurityContext {
  userId: number;
  token: string;
  username: string;
  roles: string[];
  authorities: string[];
  isAuthenticated: boolean;
}

export interface AuthObj {
  username: string,
  password: string
}

export interface NewUser {
  username: string,
  password: string,
  passwordConfirm: string,
  email: string,
  firstName: string,
  middleName: string,
  lastName: string
}

@Injectable()
export class AuthorisationService {
  securityCxt: SecurityContext;

  constructor(
    private storageService: StorageService,
    private http: Http
  ) {
    this.securityCxt = {
      userId: -1,
      token: undefined,
      username: 'guest',
      roles: ['guest'],
      authorities: [],
      isAuthenticated: false
    };
  }

  /**
   * Login function - based on _$http service
   * @param  {[Object]} authObj      [Contains user data - username and password]
   * @param  {[Function]} success [Function to be executed in case of success]
   * @param  {[Function]} err     [Function to be executed in case of error]
   */
  public login(authObj: AuthObj, success, err) {
    let body = AuthorisationService.constructFormData(authObj);;

    return this.http.post(`${BASE_URL}` + '/login', body, HEADER)
      .map(res => res.json())
      .toPromise()
      .then((resp) => {
        // Update 'securityCxt' with logged user data
        this.resetSecurityContext(resp);
        success(true);
      })
      .catch(resp => {
        // Clear 'securityCxt' (revert to 'guest' access)
        this.resetSecurityContext(null);
        // Execute error callback passed as parameter to the function
        err(JSON.parse(resp._body));
      });
  }

  public register(newUser: NewUser, succes, err) {
    let body = AuthorisationService.constructFormData(newUser);

    return this.http.post(`${BASE_URL}` + '/register', body, HEADER)
      .map(res => res.json())
      .toPromise()
      .then((resp) => {
        succes(resp);
      })
      .catch(resp => {
        err(JSON.parse(resp._body));
      })
  }

  public hasAuthority(authority: string) {
    for (let i = 0; i < this.securityCxt.authorities.length; i++) {
      if(this.securityCxt.authorities[i] === authority) {
        return true;
      }
    }
    return false;
  }

  public resetSecurityContext(authorisation: SecurityContext) {
    if (authorisation) {
      this.securityCxt.userId = authorisation.userId;
      this.securityCxt.token = authorisation.token;
      this.securityCxt.username = authorisation.username;
      this.securityCxt.roles = authorisation.roles;
      this.securityCxt.authorities = authorisation.authorities;
      this.securityCxt.isAuthenticated = true;
    } else {
      this.securityCxt.userId = -1;
      this.securityCxt.token = undefined;
      this.securityCxt.username = 'guest';
      this.securityCxt.roles = ['guest'];
      this.securityCxt.authorities = [];
      this.securityCxt.isAuthenticated = false;
    }
    this.storageService.setSecurityContext(this.securityCxt);
  };

  private static constructFormData(obj) {
    let result: string = "";

    for (let property in obj) {
      if (obj.hasOwnProperty(property)) {
        result += property + "=" + obj[property] + "&";
      }
    }
    if(result.length > 0) {
      return result.substring(0, result.length-1);
    }
    return result;
  }
}
