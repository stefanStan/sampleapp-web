import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {StorageService} from "./storage.service";

const BASE_URL = 'http://localhost:8080/sampleapp-private-api/api/rest/cxf/secure/users';

@Injectable()
export class UserService {

  constructor(
    private http: Http,
    private storageService: StorageService
  ) {

  }

  /**
   * Login function - based on _$http service
   * @param  {[Object]} userId
   * @param  {[Function]} success [Function to be executed in case of success]
   * @param  {[Function]} err     [Function to be executed in case of error]
   */
  public getUserRoles(userId: number, success, err) {
    let headers = new Headers();
    headers.append('Cookie', "JSESSIONID=" + this.storageService.getToken());

    return this.http.get(`${BASE_URL}` + '/' + userId + '/roles', {headers: headers})
      .map(res => res.json())
      .toPromise()
      .then((resp) => {
        // Update 'securityCxt' with logged user data
        success(resp);
      })
      .catch(resp => {
        // Execute error callback passed as parameter to the function
        err(JSON.parse(resp._body));
      });
  }

  public setUserRoles(userId: number, roles, success, err) {
    let headers = new Headers();
    headers.append("JSESSIONID", this.storageService.getToken());

    return this.http.put(`${BASE_URL}` + '/' + userId + '/roles', roles, headers)
      .map(res => res.json())
      .toPromise()
      .then((resp) => {
        // Update 'securityCxt' with logged user data
        success(resp);
      })
      .catch(resp => {
        // Execute error callback passed as parameter to the function
        err(JSON.parse(resp._body));
      });
  }
}
