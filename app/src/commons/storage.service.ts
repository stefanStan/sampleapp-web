import {Injectable} from "@angular/core";
import {SecurityContext} from "./authorisation.service";

@Injectable()
export class StorageService {
  static SECURITY_CONTEXT_KEY: string = "securityContext";

  setSecurityContext(secContext: SecurityContext) {
    localStorage.setItem(StorageService.SECURITY_CONTEXT_KEY, JSON.stringify(secContext));
  }

  getSecurityContext(): SecurityContext {
    return JSON.parse(localStorage.getItem(StorageService.SECURITY_CONTEXT_KEY));
  }

  getToken() {
    return this.getSecurityContext().token;
  }
}
