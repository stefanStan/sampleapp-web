import { Routes } from '@angular/router';
import { Login } from './src/auth/login/login.component';
import { Register } from "./src/auth/register/register.component";
import { RolesManagement } from "./src/management/roles/roles.management.component";

let appId = "sampleapp";

let login = appId + "/login";
let register = appId + "/register";
let roles = appId + "/roles";

export const routes: Routes = [
  {path: login,       component: Login },
  {path: register,    component: Register },
  {path: roles,       component: RolesManagement}
  // {path: '*',           component: Items }
];
